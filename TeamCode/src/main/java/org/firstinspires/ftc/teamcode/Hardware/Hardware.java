package org.firstinspires.ftc.teamcode.Hardware;

import org.firstinspires.ftc.robotcore.external.Telemetry;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer;
import org.firstinspires.ftc.robotcore.external.tfod.TFObjectDetector;

import com.qualcomm.hardware.bosch.JustLoggingAccelerationIntegrator;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.HardwareMap;
import com.qualcomm.robotcore.hardware.ColorSensor;
import com.qualcomm.robotcore.hardware.CRServo;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.hardware.bosch.BNO055IMU;

//--------------------------------------------------------------
//Class for all of our hardware
//--------------------------------------------------------------
public class Hardware {

    //--------------------------------------------------------------
    //Motors
    //--------------------------------------------------------------
    public static DcMotor frontRight;
    public static DcMotor frontLeft;
    public static DcMotor backRight;
    public static DcMotor backLeft;
    static DcMotor rightIn;
    static DcMotor leftIn;
    static DcMotor slider;
    static DcMotor lift;

    //--------------------------------------------------------------
    //Servos
    //--------------------------------------------------------------
    public static CRServo grab;
    static Servo foundation;
    static Servo arm;

    //--------------------------------------------------------------
    //Sensors
    //--------------------------------------------------------------
    public static ColorSensor blockSensor;
    public static ColorSensor colorSensor;
    public static BNO055IMU imu;

    //--------------------------------------------------------------
    //TFOD
    //--------------------------------------------------------------
    public static VuforiaLocalizer vuforia;
    public static TFObjectDetector TFOD;

    public static final String SKYSTONE = "Skystone";

    //--------------------------------------------------------------
    //Driving Variables
    //--------------------------------------------------------------
    private static final double FAST_SPEED = 0.7;
    private static final double SLOW_SPEED = 0.35;
    static final double FAST_CONVERT = 1.0 / FAST_SPEED;
    static final double SLOW_CONVERT = 1.0 / SLOW_SPEED;

    //--------------------------------------------------------------
    //Intake Variables
    //--------------------------------------------------------------
    static final double INTAKE_SPEED = 1.0;

    //--------------------------------------------------------------
    //Servo Variables
    //--------------------------------------------------------------
    static final double FOUNDATION_DOWN = 0.65;
    static final double FOUNDATION_UP = 0.0;
    static final double CLAW_CLOSE = -1.0;
    static final double CLAW_OPEN = 1.0;
    public static final double ARM_DOWN = 0.22;
    public static final double ARM_UP = 0.88;

    //--------------------------------------------------------------
    //Lift Variables
    //--------------------------------------------------------------
    static final double MAX_LIFT = 0.7;
    static final double MIN_LIFT = -0.7;

    public static final String VUFORIA_KEY = "AWydOn3/////AAAAGWB2YP4r2ERKmLdFMt7DzdUYnt2f97VKdK1fMvb8c5p8iGeDLgwB9dic+osr9GAHQK3K4uJV/8yxon7KXrJNbgzKN82yuHucjwS7gmWkItkoSB+nTn/66dfKF6OyRhh7vBtZqg70Tpv3Pq75kIeij++F34cQNAA3fWEzIoPnuQkew/QP1NNjyZtnIY4lYZFEHgljmtmIP7qwM5vw5pIQRriTaDAfwWPJ9tJVa4yn8eOfPi/bdJzu7VmH9RxySYlnxImCN/EVXcSRPPPQxtjFxza/+aXM3dvRtsGfBuxfBB9YLsKR9RP6sqLG1hB+oXkjxfDDhNLdF3uMsDNy4GGJGFHewgATWnF5xXWDugOq9asb";

    public void init(HardwareMap hwMap, Telemetry tel) {

        //--------------------------------------------------------------
        //Motors
        //--------------------------------------------------------------
        frontRight = hwMap.get(DcMotor.class, "frontRight");
        frontLeft = hwMap.get(DcMotor.class, "frontLeft");
        backRight = hwMap.get(DcMotor.class, "backRight");
        backLeft = hwMap.get(DcMotor.class, "backLeft");
        rightIn = hwMap.get(DcMotor.class, "rightIn");
        leftIn = hwMap.get(DcMotor.class, "leftIn");
        slider = hwMap.get(DcMotor.class, "slider");
        lift = hwMap.get(DcMotor.class, "lift");

        frontRight.setDirection(DcMotorSimple.Direction.REVERSE);
        frontLeft.setDirection(DcMotorSimple.Direction.FORWARD);
        backRight.setDirection(DcMotorSimple.Direction.REVERSE);
        backLeft.setDirection(DcMotorSimple.Direction.FORWARD);
        rightIn.setDirection(DcMotorSimple.Direction.FORWARD);
        leftIn.setDirection(DcMotorSimple.Direction.FORWARD);
        slider.setDirection(DcMotorSimple.Direction.FORWARD);
        lift.setDirection(DcMotorSimple.Direction.FORWARD);

        frontRight.setPower(0.0);
        frontLeft.setPower(0.0);
        backRight.setPower(0.0);
        backLeft.setPower(0.0);
        rightIn.setPower(0.0);
        leftIn.setPower(0.0);
        slider.setPower(0.0);
        lift.setPower(0.0);

        frontRight.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        frontLeft.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        backRight.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        backLeft.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        rightIn.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        leftIn.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        slider.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        lift.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        tel.addData("Motors", "Done");
        tel.update();

        //--------------------------------------------------------------
        //Servos
        //--------------------------------------------------------------
        foundation = hwMap.get(Servo.class, "foundation");
        grab = hwMap.get(CRServo.class, "grab");
        arm = hwMap.get(Servo.class, "arm");

        foundation.setDirection(Servo.Direction.FORWARD);
        foundation.setPosition(0.0);
        arm.setDirection(Servo.Direction.FORWARD);
        arm.setPosition(ARM_UP);
        grab.setPower(0.0);
        tel.addData("Servo", "Done");
        tel.update();

        //--------------------------------------------------------------
        //Sensors
        //--------------------------------------------------------------
        BNO055IMU.Parameters parameters = new BNO055IMU.Parameters();
        parameters.angleUnit           = BNO055IMU.AngleUnit.DEGREES;
        parameters.accelUnit           = BNO055IMU.AccelUnit.METERS_PERSEC_PERSEC;
        parameters.calibrationDataFile = "BNO055IMUCalibration.json";
        parameters.loggingEnabled      = true;
        parameters.loggingTag          = "IMU";
        parameters.accelerationIntegrationAlgorithm = new JustLoggingAccelerationIntegrator();

        imu = hwMap.get(BNO055IMU.class, "imu");
        imu.initialize(parameters);

        colorSensor = hwMap.get(ColorSensor.class, "color");
        blockSensor = hwMap.get(ColorSensor.class, "block");
        colorSensor.enableLed(true);
        blockSensor.enableLed(true);
    }
}
