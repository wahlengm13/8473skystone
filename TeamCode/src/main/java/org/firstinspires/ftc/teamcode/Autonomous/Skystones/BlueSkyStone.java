package org.firstinspires.ftc.teamcode.Autonomous.Skystones;

import static org.firstinspires.ftc.teamcode.Hardware.Hardware.colorSensor;

import org.firstinspires.ftc.robotcore.external.navigation.VuforiaTrackable;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaTrackableDefaultListener;
import org.firstinspires.ftc.teamcode.Autonomous.Auto;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;

@Autonomous(name = "Blue Skystone")
public class BlueSkyStone extends Auto {
    @Override
    protected void information() {
        currentAlliance = alliance.BLUE;
        currentSide = side.SKYSTONE;
    }

    @Override
    protected void autoCode() {
        drive(1.075, 0.5f);
        off();
        timer.reset();
        while (timer.seconds() < 2) {
            for (VuforiaTrackable trackable : allTrackables) {
                if (((VuforiaTrackableDefaultListener) trackable.getListener()).isVisible()) {
                    telemetry.addData("Visible Target", trackable.getName());
                    telemetry.update();
                    if (trackable.getName().equals("Stone Target")) location = "LEFT";
                    break;
                }
            }
        }
        if (location.equals("")) {
            drive(0.0f, -0.5f, 0.45);
            off();
        }
        timer.reset();
        while (timer.seconds() < 2 && location.equals("")) {
            for (VuforiaTrackable trackable : allTrackables) {
                if (((VuforiaTrackableDefaultListener) trackable.getListener()).isVisible()) {
                    telemetry.addData("Visible Target", trackable.getName());
                    telemetry.update();
                    if (trackable.getName().equals("Stone Target")) location = "CENTER";
                    break;
                }
            }
        }
        if (!location.equals("")) {
            drive(0.5f, 0.05f, 0.7);
            off();
            grab();
            delay(1);
            color = colorSensor.blue() + 2;
            grabPower(-1.0);
            drive(1.1, -0.5f);
            grabPower(0.0);

            while(colorSensor.blue() <= color && opModeIsActive()) {
                drive(0.5f);
            }
            delay(1.5);

            drive(0.25, 0.5f);
            grab();
            delay(0.25);
            drive(0.5, -0.5f);
        } else {
            drive(0.55f, -0.25f, 1);
            off();
            grab();
            delay(1);
            color = colorSensor.blue() + 4;
            grabPower(-1.0);
            drive(-0.55f, 0.25f, 1.5);
            grabPower(0.0);

            while(colorSensor.blue() <= color && opModeIsActive()) {
                drive(0.5f);
            }
            delay(1.5);

            drive(0.25, 0.5f);
            grab();
            delay(0.25);
            drive(0.5, -0.5f);
        }

        if (location.equals("LEFT")) {

        } else if (location.equals("CENTER")) {

        } else {

        }

        while (colorSensor.blue() <= color - 2 && opModeIsActive()) {
            drive(-0.5f);
        }
        drive(0.5f);
        delay(0.5);
        off();
        flashlight(false);
    }
}
