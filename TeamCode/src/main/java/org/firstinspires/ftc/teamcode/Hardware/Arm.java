package org.firstinspires.ftc.teamcode.Hardware;

import org.firstinspires.ftc.teamcode.Methods.Wait;

import static org.firstinspires.ftc.teamcode.Hardware.Hardware.ARM_DOWN;
import static org.firstinspires.ftc.teamcode.Hardware.Hardware.ARM_UP;
import static org.firstinspires.ftc.teamcode.Hardware.Hardware.arm;

//--------------------------------------------------------------
//Class for our autonomous grabbing mechanism
//--------------------------------------------------------------
public class Arm {
    private int currentPos = 0;

    public void armMove() {
        switch(currentPos) {
            case 0:
                arm.setPosition(ARM_DOWN);
                ++currentPos;
                break;
            case 1:
                arm.setPosition(ARM_UP);
                --currentPos;
                break;
        }
    }
    public void armMove(double pos){
        arm.setPosition(pos);
    }
}
