package org.firstinspires.ftc.teamcode.Tests;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

import org.firstinspires.ftc.teamcode.Hardware.Hardware;

import static org.firstinspires.ftc.teamcode.Hardware.Hardware.colorSensor;

@TeleOp(name = "TestColor")
@Disabled
public class ColorSensor extends LinearOpMode {
    private Hardware ezra = new Hardware();
    @Override
    public void runOpMode(){
        ezra.init(hardwareMap, telemetry);

        waitForStart();
        while (opModeIsActive()) {
            telemetry.addData("Red", colorSensor.red());
            telemetry.addData("Blue", colorSensor.blue());
            telemetry.update();
        }
    }
}
