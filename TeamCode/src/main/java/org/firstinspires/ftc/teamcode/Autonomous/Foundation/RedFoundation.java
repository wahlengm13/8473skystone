package org.firstinspires.ftc.teamcode.Autonomous.Foundation;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import org.firstinspires.ftc.teamcode.Autonomous.Auto;

import static org.firstinspires.ftc.teamcode.Hardware.Hardware.colorSensor;

@Autonomous(name = "Red Foundation")
public class RedFoundation extends Auto {
    @Override
    protected void information() {
        currentAlliance = alliance.RED;
        currentSide = side.FOUNDATION;
    }

    @Override
    public void autoCode() {
        drive(-0.5f, 0.184545f, 2);
        off();
        fGrab();
        delay(2);
        drive(10, 0.5f);
        fGrab();

        grabPower(-1.0);
        color = colorSensor.red() + 5;
        drive(0.0f, -0.5f, 1);
        grabPower(0.0);
        while(colorSensor.red() <= color && opModeIsActive()) {
            drive(-0.5f);
        }
        drive(0.0f, 0.2f, 1);
        off();
    }
}
