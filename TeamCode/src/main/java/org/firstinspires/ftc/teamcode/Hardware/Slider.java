package org.firstinspires.ftc.teamcode.Hardware;

import static org.firstinspires.ftc.teamcode.Hardware.Hardware.slider;

public class Slider {

    public void in() {
        slider.setPower(1.0);
    }
    public void out() {
        slider.setPower(-1.0);
    }
    public void off() {
        slider.setPower(0.0);
    }
}
