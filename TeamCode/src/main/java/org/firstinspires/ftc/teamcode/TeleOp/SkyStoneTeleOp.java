package org.firstinspires.ftc.teamcode.TeleOp;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

import org.firstinspires.ftc.robotcore.external.navigation.Acceleration;
import org.firstinspires.ftc.teamcode.Hardware.Arm;
import org.firstinspires.ftc.teamcode.Hardware.Driving;
import org.firstinspires.ftc.teamcode.Hardware.Foundation;
import org.firstinspires.ftc.teamcode.Hardware.Grabber;
import org.firstinspires.ftc.teamcode.Hardware.Hardware;
import org.firstinspires.ftc.teamcode.Hardware.Intake;
import org.firstinspires.ftc.teamcode.Hardware.Lift;
import org.firstinspires.ftc.teamcode.Hardware.Slider;

import static org.firstinspires.ftc.teamcode.Hardware.Hardware.imu;

@TeleOp(name = "SkyStoneTeleOp")
public class SkyStoneTeleOp extends LinearOpMode {
    private Foundation foundation = new Foundation();
    private Hardware ezra = new Hardware();
    private Driving drive = new Driving();
    private Grabber grab = new Grabber();
    private Slider slider = new Slider();
    private Intake in = new Intake();
    private Lift lift = new Lift();
    private Arm arm = new Arm();

    private boolean rb1 = false, xb = false, bb = false;
    private float x, y, z, y2;

    Acceleration gravity;

    @Override
    public void runOpMode(){
        ezra.init(hardwareMap, telemetry);

        waitForStart();
        while (opModeIsActive()) {
            gravity = imu.getGravity();
            telemetry.addData("Acceleration", gravity.xAccel);
            telemetry.addData("Velocity", imu.getVelocity());
            telemetry.update();
            //--------------------------------------------------------------
            //Driving
            //--------------------------------------------------------------
            getJoyValues();
            drive.drive(x, y, z);
            if (gamepad1.right_bumper && !rb1) drive.speedChange();

            //--------------------------------------------------------------
            //Intake
            //--------------------------------------------------------------
            if (gamepad2.right_bumper) in.in();
            else if (gamepad2.left_bumper) in.out();
            else in.off();

            //--------------------------------------------------------------
            //Slider
            //--------------------------------------------------------------
            if(gamepad2.y) slider.in();
            else if (gamepad2.a) slider.out();
            else slider.off();

            //--------------------------------------------------------------
            //Claw
            //--------------------------------------------------------------
            if (gamepad2.left_trigger != 0.0) grab.open();
            else if(gamepad2.right_trigger != 0.0) grab.close();
            else grab.off();

            //--------------------------------------------------------------
            //Arm
            //--------------------------------------------------------------
            if (gamepad2.x && !xb) arm.armMove();

            //--------------------------------------------------------------
            //Foundation
            //--------------------------------------------------------------
            if (gamepad2.b && !bb) foundation.move();

            //--------------------------------------------------------------
            //Lift
            //--------------------------------------------------------------
            if (!(gamepad2.left_stick_y == 0.0)) lift.lift(y2);
            else lift.lift(0.0f);

            //--------------------------------------------------------------
            //Buttons
            //--------------------------------------------------------------
            rb1 = gamepad1.right_bumper;
            bb = gamepad2.b;
            xb = gamepad2.x;
        }
    }
    private void getJoyValues() {
        y = gamepad1.left_stick_y;
        x = -gamepad1.left_stick_x;
        z = gamepad1.right_stick_x;
        y2 = -gamepad2.left_stick_y;
    }
}