package org.firstinspires.ftc.teamcode.Hardware;

import org.firstinspires.ftc.teamcode.Methods.Wait;

import static org.firstinspires.ftc.teamcode.Hardware.Hardware.INTAKE_SPEED;
import static org.firstinspires.ftc.teamcode.Hardware.Hardware.leftIn;
import static org.firstinspires.ftc.teamcode.Hardware.Hardware.rightIn;

//--------------------------------------------------------------
//Class for our Intake
//--------------------------------------------------------------
public class Intake {
    private static int currentState = 0;

    public void change() {
        switch (currentState) {
            case 0:
                rightIn.setPower(INTAKE_SPEED);
                leftIn.setPower(INTAKE_SPEED);
                ++currentState;
                break;
            case 1:
                rightIn.setPower(0.0);
                leftIn.setPower(0.0);
                --currentState;
                break;
        }
    }

    public void in() {
        rightIn.setPower(INTAKE_SPEED);
        leftIn.setPower(INTAKE_SPEED);
    }

    public void out() {
        rightIn.setPower(-INTAKE_SPEED);
        leftIn.setPower(-INTAKE_SPEED);
    }
    public void off() {
        rightIn.setPower(0.0);
        leftIn.setPower(0.0);
    }
}