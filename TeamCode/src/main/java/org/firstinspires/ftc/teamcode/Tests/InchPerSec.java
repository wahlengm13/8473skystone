package org.firstinspires.ftc.teamcode.Tests;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;

import org.firstinspires.ftc.teamcode.Autonomous.Auto;

@Autonomous(name = "TestIMU")
@Disabled
public class InchPerSec extends Auto {
    @Override
    protected void information() {
        currentAlliance = alliance.BLUE;
        currentSide = side.FOUNDATION;
    }

    @Override
    protected void autoCode() {

    }
}
