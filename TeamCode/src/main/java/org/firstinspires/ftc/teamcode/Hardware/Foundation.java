package org.firstinspires.ftc.teamcode.Hardware;

import org.firstinspires.ftc.robotcore.external.Telemetry;

import static org.firstinspires.ftc.teamcode.Hardware.Hardware.FOUNDATION_DOWN;
import static org.firstinspires.ftc.teamcode.Hardware.Hardware.FOUNDATION_UP;
import static org.firstinspires.ftc.teamcode.Hardware.Hardware.foundation;

public class Foundation {
    private int currentPos = 0;
    public void move() {
        switch (currentPos) {
            case 0 :
                foundation.setPosition(FOUNDATION_DOWN);
                currentPos++;
                break;
            case 1 :
                foundation.setPosition(FOUNDATION_UP);
                currentPos--;
                break;
        }
    }

    public void setPos (double pos) {
        foundation.setPosition(pos);
    }
}
