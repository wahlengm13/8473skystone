package org.firstinspires.ftc.teamcode.Methods;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.util.ElapsedTime;

/**
 * Command to wait for a specified amount of time
 */
@Disabled
public class Wait {
    private ElapsedTime timer = new ElapsedTime();

    public void milliseconds(int time) {
        timer.reset();
        while(timer.milliseconds() <= time) {
            timer.time();
        }
    }
    public void seconds(double time) {
        timer.reset();
        while (timer.seconds() <= time){
            timer.time();
        }
    }
}
