package org.firstinspires.ftc.teamcode.Autonomous;

import com.vuforia.CameraDevice;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.robotcore.external.ClassFactory;
import org.firstinspires.ftc.robotcore.external.navigation.AngleUnit;
import org.firstinspires.ftc.robotcore.external.navigation.AxesOrder;
import org.firstinspires.ftc.robotcore.external.navigation.AxesReference;
import org.firstinspires.ftc.robotcore.external.navigation.Orientation;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaTrackable;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaTrackableDefaultListener;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaTrackables;
import org.firstinspires.ftc.teamcode.Hardware.Arm;
import org.firstinspires.ftc.teamcode.Hardware.Driving;
import org.firstinspires.ftc.teamcode.Hardware.Foundation;
import org.firstinspires.ftc.teamcode.Hardware.Hardware;

import java.util.ArrayList;
import java.util.List;

import static org.firstinspires.ftc.teamcode.Hardware.Hardware.ARM_DOWN;
import static org.firstinspires.ftc.teamcode.Hardware.Hardware.ARM_UP;
import static org.firstinspires.ftc.teamcode.Hardware.Hardware.VUFORIA_KEY;
import static org.firstinspires.ftc.teamcode.Hardware.Hardware.imu;

public abstract class Auto extends LinearOpMode {
    private Foundation foundation = new Foundation();
    private ElapsedTime autoTime = new ElapsedTime();
    protected ElapsedTime timer = new ElapsedTime();
    private Hardware ezra = new Hardware();
    private Driving drive = new Driving();
    private Arm arm = new Arm();

    private double angleIE, lastAE;
    private int currentState = 0;
    protected int color;

    protected alliance currentAlliance;
    protected side currentSide;

    protected List<VuforiaTrackable> allTrackables = new ArrayList<>();
    protected String location = "";

    //--------------------------------------------------------------
    //OpMode Stuff
    //--------------------------------------------------------------
    protected abstract void information();
    protected abstract void autoCode();

    @Override
    public void runOpMode() {
        ezra.init(hardwareMap, telemetry);
        information();
        color = currentAlliance == alliance.RED ? 24 : 17;
        if (currentSide == side.SKYSTONE) {
            vfInit();
            while (!opModeIsActive() && !isStopRequested()) {
                for (VuforiaTrackable trackable : allTrackables) {
                    if (((VuforiaTrackableDefaultListener) trackable.getListener()).isVisible()) {
                        telemetry.addData("Visible Target", trackable.getName());
                        break;
                    }
                }
                telemetry.update();
            }
        } else {
            telemetry.addData("Ready ", " Press > to start");
            telemetry.update();
        }

        waitForStart();
        autoCode();
        while(opModeIsActive()) {
            idle();
        }
    }

    //--------------------------------------------------------------
    //Initialization Stuff
    //--------------------------------------------------------------
    private void vfInit() {
        int cameraMonitorView =  hardwareMap.appContext.getResources().getIdentifier("cameraMonitorViewId", "id", hardwareMap.appContext.getPackageName());
        VuforiaLocalizer.Parameters parameters = new VuforiaLocalizer.Parameters(cameraMonitorView);

        parameters.vuforiaLicenseKey = VUFORIA_KEY;
        parameters.cameraDirection = VuforiaLocalizer.CameraDirection.BACK;

        Hardware.vuforia = ClassFactory.getInstance().createVuforia(parameters);

        VuforiaTrackables targetsSkyStone = Hardware.vuforia.loadTrackablesFromAsset("Skystone");
        VuforiaTrackable stoneTarget = targetsSkyStone.get(0);
        stoneTarget.setName("Stone Target");

        allTrackables.addAll(targetsSkyStone);

        targetsSkyStone.activate();
        flashlight(true);
    }


    /**
     * This method is used to turn on and off the flashlight of the phone to make the lighting more true for vuforia.
     * @param enabled true/false to set the led on/off.
     */
    protected void flashlight(boolean enabled) {
        CameraDevice.getInstance().setFlashTorchMode(enabled);
    }

    //--------------------------------------------------------------
    //PID Loops
    //--------------------------------------------------------------

    /**
     * This method is used to keep the robot straight while it moves.
     * @return correction for turning
     */
    private float PID() {
        Orientation angles = imu.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZYX, AngleUnit.DEGREES);

        double angleE = angles.firstAngle;
        angleIE += angleE;
        double deltaAE = angleE - lastAE;

        double PTerm = 0.035 * angleE;
        double ITerm = 0.0 * angleIE;
        double DTerm = 0.07 * deltaAE;

        double correction = PTerm + ITerm + DTerm;
        correction = Math.min(0.35, Math.max(-0.35, correction));
        lastAE = angleE;
        return (float)correction;
    }

    //--------------------------------------------------------------
    //Driving Methods
    //--------------------------------------------------------------
    protected void drive(float x, float y, double time) {
        autoTime.reset();
        while (autoTime.seconds() < time && opModeIsActive()) {
            drive.drive(x, y, PID());
        }
    }
    protected void drive(double time, float x) {
        autoTime.reset();
        while (autoTime.seconds() < time && opModeIsActive()) {
            drive.drive(x, 0.0f, PID());
        }
    }
    protected void drive(float y) {
        //Travel y-axis
        drive.drive(0.0f, y, PID());
    }
    protected void off() {
        //Stop
        drive.drive(0.0f, 0.0f, 0.0f);
    }

    //--------------------------------------------------------------
    //Grabbing Method
    //--------------------------------------------------------------
    protected void grab () {
        switch (currentState) {
            case 0:
                arm.armMove(ARM_DOWN);
                ++currentState;
                break;
            case 1:
                arm.armMove(ARM_UP);
                --currentState;
                break;
        }
    }
    protected void fGrab() {
        foundation.move();
    }
    protected void grabPower(double power) {
        Hardware.grab.setPower(power);
    }

    /**
     * This method is for waiting a certain time.
     * @param time in seconds to wait.
     */
    protected void delay(double time) {
        autoTime.reset();
        while (autoTime.seconds() < time && opModeIsActive()) {
            telemetry.addData("Time", autoTime.seconds());
            telemetry.update();
        }
    }

    protected enum alliance {
        RED, BLUE
    }

    protected enum side {
        SKYSTONE, FOUNDATION
    }
}