package org.firstinspires.ftc.teamcode.Autonomous.Foundation;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import org.firstinspires.ftc.teamcode.Autonomous.Auto;

import static org.firstinspires.ftc.teamcode.Hardware.Hardware.colorSensor;

@Autonomous(name = "Blue Foundation")
public class BlueFoundation extends Auto {
    @Override
    protected void information() {
        currentAlliance = alliance.BLUE;
        currentSide = side.FOUNDATION;
    }

    @Override
    public void autoCode() {
        drive(-0.5f, -0.2f, 1.5);
        drive(0.5, -0.5f);
        off();
        fGrab();
        delay(2);
        drive(0.5f, -0.2f, 10);
        fGrab();

        drive(0.0f, 0.5f, 1);
        color = colorSensor.blue() + 4;
        grabPower(-1.0);
        delay(0.5);
        grabPower(0.0);
        while(colorSensor.blue() <= color && opModeIsActive()) {
            drive(0.5f);
        }
        off();
    }
}
