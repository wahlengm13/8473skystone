package org.firstinspires.ftc.teamcode.Tests;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import org.firstinspires.ftc.teamcode.Hardware.Hardware;
import static org.firstinspires.ftc.teamcode.Hardware.Hardware.blockSensor;
import static org.firstinspires.ftc.teamcode.Hardware.Hardware.colorSensor;

@TeleOp(name = "ColorTest")
@Disabled
public class ColorSensorTest extends OpMode {
    private Hardware ezra = new Hardware();
    @Override
    public void init() {
        ezra.init(hardwareMap, telemetry);
    }

    @Override
    public void loop() {
        telemetry.addData("Red", colorSensor.red());
        telemetry.addData("Blue", colorSensor.blue());
        telemetry.addData("Green", blockSensor.green());
        telemetry.update();
    }
}
