package org.firstinspires.ftc.teamcode.Tests;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

import org.firstinspires.ftc.robotcore.external.tfod.Recognition;
import org.firstinspires.ftc.teamcode.Autonomous.Auto;

import java.util.List;

import static org.firstinspires.ftc.teamcode.Hardware.Hardware.SKYSTONE;
import static org.firstinspires.ftc.teamcode.Hardware.Hardware.TFOD;

@TeleOp (name = "TensorTest")
@Disabled
public class TensorFlowTest extends Auto {

    @Override
    protected void information() {
        currentAlliance = alliance.BLUE;
        currentSide = side.SKYSTONE;
    }

    @Override
    protected void autoCode() {
        drive(0.5, 0.3f);
        off();
        OpMode:
        while (opModeIsActive()) {
            if (TFOD != null) {
                TFOD.activate();
                List<Recognition> updatedRecognitions = TFOD.getUpdatedRecognitions();
                if (updatedRecognitions != null) {
                    for (Recognition recognition : updatedRecognitions) {
                        if (recognition.getLabel().equalsIgnoreCase(SKYSTONE)) {
                            telemetry.addData("Right", recognition.getRight());
                            telemetry.addData("Left", recognition.getLeft());
                            telemetry.addData("Center", (recognition.getLeft() + recognition.getRight()) / 2);
                            telemetry.addData("Confidence", recognition.getConfidence());
                            telemetry.update();
                            break OpMode;
                        }
                    }
                }
            }
        }
    }
}