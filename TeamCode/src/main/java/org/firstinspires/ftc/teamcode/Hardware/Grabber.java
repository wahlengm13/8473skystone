package org.firstinspires.ftc.teamcode.Hardware;

import static org.firstinspires.ftc.teamcode.Hardware.Hardware.CLAW_CLOSE;
import static org.firstinspires.ftc.teamcode.Hardware.Hardware.CLAW_OPEN;
import static org.firstinspires.ftc.teamcode.Hardware.Hardware.grab;

public class Grabber {
    //--------------------------------------------------------------
    //Close the claw
    //--------------------------------------------------------------
    public void close() {
        grab.setPower(CLAW_CLOSE);
    }

    //--------------------------------------------------------------
    //Open the claw
    //--------------------------------------------------------------
    public void open() {
        grab.setPower(CLAW_OPEN);
    }

    //--------------------------------------------------------------
    //Stop claw
    //--------------------------------------------------------------
    public void off() {
        grab.setPower(0.0);
    }
}
