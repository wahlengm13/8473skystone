package org.firstinspires.ftc.teamcode.Hardware;

import com.qualcomm.robotcore.util.Range;

import static org.firstinspires.ftc.teamcode.Hardware.Hardware.MAX_LIFT;
import static org.firstinspires.ftc.teamcode.Hardware.Hardware.MIN_LIFT;
import static org.firstinspires.ftc.teamcode.Hardware.Hardware.lift;

//--------------------------------------------------------------
//Class for our lift
//--------------------------------------------------------------
public class Lift {
    public void lift(float power) {
        lift.setPower(Range.clip(power, MIN_LIFT, MAX_LIFT));
    }
}
