package org.firstinspires.ftc.teamcode.TeleOp;

import android.os.Environment;

import com.qualcomm.robotcore.eventloop.opmode.*;

import org.firstinspires.ftc.teamcode.Hardware.Arm;
import org.firstinspires.ftc.teamcode.Hardware.Grabber;
import org.firstinspires.ftc.teamcode.Hardware.Driving;
import org.firstinspires.ftc.teamcode.Hardware.Hardware;
import org.firstinspires.ftc.teamcode.Hardware.Intake;
import org.firstinspires.ftc.teamcode.Hardware.Lift;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import static org.firstinspires.ftc.teamcode.Hardware.Hardware.blockSensor;
import static org.firstinspires.ftc.teamcode.Hardware.Hardware.frontRight;

//--------------------------------------------------------------
//Our SkyStoneTeleOp class
//--------------------------------------------------------------
@TeleOp(name = "SkyStoneTeleOpOld")
@Disabled
public class SkyStoneTeleOpOld extends OpMode {

    private String fileName = "test.txt";
    File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), fileName);
    private String fileContent = "321tset";
    String temp = "";

    private Hardware ezra = new Hardware();
    private Driving drive = new Driving();
    private Grabber grab = new Grabber();
    private Intake in = new Intake();
    private Lift lift = new Lift();
    private Arm arm = new Arm();

    private float x, y, z, y2;
    private boolean block = true;

    private boolean rb1 = false, rb2 = false, xb = false;

    @Override
    public void init() {
        ezra.init(hardwareMap, telemetry);
    }

    @Override
    public void loop(){
        //--------------------------------------------------------------
        //Driving
        //--------------------------------------------------------------
        getJoyValues();
        drive.drive(x, y, z);
        if (gamepad1.right_bumper && !rb1) drive.speedChange();

        //--------------------------------------------------------------
        //Intake
        //--------------------------------------------------------------
        if(blockSensor.red() > 30 && !block) {
            grab.close();
            in.in();
            block = true;
        }
        if (gamepad2.right_bumper && !rb2) {
            in.in();
            block = false;
        }

        //--------------------------------------------------------------
        //Claw
        //--------------------------------------------------------------
        if(gamepad2.a) {
            grab.open();
            block = false;
        }
        else if (!block) grab.off();

        //--------------------------------------------------------------
        //Arm
        //--------------------------------------------------------------
        if (gamepad2.x && xb) arm.armMove();

        //--------------------------------------------------------------
        //Lift
        //--------------------------------------------------------------
        if (!(gamepad2.left_stick_y == 0.0)) lift.lift(y2);
        else lift.lift(0.0f);

        rb1 = gamepad1.right_bumper;
        rb2 = gamepad2.right_bumper;
        xb = gamepad2.x;


    }
    private void getJoyValues() {
        y = gamepad1.left_stick_y;
        x = -gamepad1.left_stick_x;
        z = gamepad1.right_stick_x;
        y2 = gamepad2.left_stick_y;
    }
}
