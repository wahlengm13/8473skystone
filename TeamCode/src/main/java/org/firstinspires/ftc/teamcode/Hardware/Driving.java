package org.firstinspires.ftc.teamcode.Hardware;

import org.firstinspires.ftc.teamcode.Methods.Wait;

import static org.firstinspires.ftc.teamcode.Hardware.Hardware.FAST_CONVERT;
import static org.firstinspires.ftc.teamcode.Hardware.Hardware.SLOW_CONVERT;
import static org.firstinspires.ftc.teamcode.Hardware.Hardware.backLeft;
import static org.firstinspires.ftc.teamcode.Hardware.Hardware.backRight;
import static org.firstinspires.ftc.teamcode.Hardware.Hardware.frontLeft;
import static org.firstinspires.ftc.teamcode.Hardware.Hardware.frontRight;

//--------------------------------------------------------------
//Class for our DriveTrain
//--------------------------------------------------------------
public class Driving {
    private Wait wait = new Wait();

    private static driveSpeed[] Speeds = {driveSpeed.FAST, driveSpeed.SLOW};
    private static int currentSpeed = 0;
    private static driveSpeed speed = Speeds[currentSpeed];

    public void drive(float x, float y, float z) {
        double CONVERT = speed == driveSpeed.FAST ? FAST_CONVERT : SLOW_CONVERT;
        frontRight.setPower((y + x + z / 2) / CONVERT);
        frontLeft.setPower((y - x - z / 2) / CONVERT);
        backRight.setPower((y - x + z / 2) / CONVERT);
        backLeft.setPower((y + x - z / 2) / CONVERT);
    }
    public void speedChange() {
        switch (currentSpeed) {
            case 0 :
                ++currentSpeed;
                speed = Speeds[currentSpeed];
                break;
            case 1 :
                --currentSpeed;
                speed = Speeds[currentSpeed];
                break;
        }
    }
    private enum driveSpeed {
        FAST, SLOW
    }
}
